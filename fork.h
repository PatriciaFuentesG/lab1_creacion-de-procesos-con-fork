#ifndef FORK_H // prevent multiple inclusions of header file, permite no unclirlo mas de una vez
#define FORK_H
//#include <string>
#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
using namespace std;


class Fork {
    private:
      pid_t pid;
      int segundos;
    public:
        // default constructor
        Fork(int seg, string URL);
  
        void crear();
        void ejecutarCodigo(string url);
       

    
};

#endif