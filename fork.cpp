#include <unistd.h>
#include <sys/wait.h>
#include "fork.h"
#include <iostream>
#include <string.h>
#include <stdlib.h> 
#include <cstdlib>
using namespace std;

Fork::Fork(int seg, string URL){
  crear();
  ejecutarCodigo( URL);
}



void Fork::crear(){
  pid = fork(); 
}

void Fork::ejecutarCodigo(string url){

  if (pid < 0) {
        cout << "No se pudo crear el proceso ...";
        cout << endl;
      
      } else if (pid == 0) {
        // Código del proceso hijo.
        cout << "ID Proceso hijo: " << getpid();
        cout << endl;
        cout << "Ejecuta código proceso hijo ...";
        cout << endl;
        cout << "Se extrae el audio..."<< endl;
        // Con el siguente comando el audio se guarda como musica.mp3
        string comando = "youtube-dl -x --audio-format mp3 --output 'musica.%(ext)s' ";
        comando += url;
        system(comando.c_str());
        
        // Espera "segundos" para continuar (para pruebas).
        sleep(segundos);
      
      } else {
        // Código proceso padre.
        // Padre espera por el término del proceso hijo.
        wait (NULL);

        cout << "Termina código de proceso hijo ...";
        cout << endl;
        cout << "Se empieza a reproducir el audio..."<<endl;
        cout << "Continua con código proceso padre: " << getpid();
        // el comando mpg123 reproduce la audio seleccionado
        string nw_comando = "mpg123 musica.mp3";
        system(nw_comando.c_str());
        
        cout << endl;
      }
}

